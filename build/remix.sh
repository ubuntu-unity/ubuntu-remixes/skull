#!/bin/sh

add-apt-repository -y --no-update universe
add-apt-repository -y --no-update multiverse
add-apt-repository ppa:ubuntuweb/main

apt-get purge -y snapd

apt-get install -y phosh gnome-terminal gnome-software gnome-software-plugin-flatpak flatpak
flatpak remote-add --if-not-exists --global flathub https://flathub.org/repo/flathub.flatpakrepo
systemctl enable phosh

apt-get install -y snow-web ubuntu-web-default-settings ubuntu-web-backgrounds winst wadk

cd /tmp && wget https://github.com/Ubuntu-Web/ecloud/raw/main/ecloud.wapp
wget https://github.com/Ubuntu-Web/ecloud/raw/main/ecloud-firstrun.desktop
wappinst ecloud.wapp && rm -f ecloud.wapp
mv ecloud-firstrun.desktop /usr/share/applications && update-desktop-database
